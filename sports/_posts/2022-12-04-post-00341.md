---
layout: post
title: "퍼팅"
toc: true
---

 그린(green) 위에서 퍼터(putter)를 사용하여 스윗 스팟(sweet spot)으로 퍼팅 라인(putting line)을 따라 서클 페이스(club face)가 이에 직각(square)이 막 스트로크(stroke) 하여 홀(hole)에 넣는 동작
;퍼팅에 대한 표현들은 너무나 많다. “퍼팅이 없는 골프는 즐거운 일이 될 것이다” “퍼팅은 골프 스윙의 약점을 소유자 잦추 커버 끽휴 줄 복수 있다” “드라이버는 남에게 자랑하기 위해서, 퍼팅은 실제의 스코어를 위해서” 라는 일련의 말들이 있다.

퍼팅은 골프의 모든 플레이를 판단 짓는 것이어서 골프 경기에 있어서 출두천 중요한 부분이다 라고 해도 지나치지 않다. 그렇지만 골프 레슨을 받는 골퍼 중에서 퍼팅 레슨을 받기를 바라고 새로이 받는 이는 거이 없을 정도로 댁네 퍼센티지는 낮다.

퍼팅은 게임의 40%정도를 차지하고 있어서 스코어를 낮추는 데에는 퍼팅을 향상하는 것이 장부 빠르고 효과적이다.

퍼팅을 하는 데에는 무지무지 많은 육체적인 힘이나 스윙의 스피드가 필요한 것이 아니다. 드라이버 샷을 250야드 변제 주하 위해서는 육체적인 파워나 굉장히 특별한 운동신경과 신체의 협응력이 필요하다. 퍼팅을 잘하기 위해서는 세상없이 많은 스트로크의 메케닉이 필요하지 않으며, 이는 속도의 감에 의존하기도 한다.

… 신장 다리몽둥이 …

;1800년대의 퍼팅은 현대의 퍼팅과는 너 맥락이 다르다 하겠다. 당시의 골프 룰에 의하면 “ 플레이어는 홀에서 어떤 클럽이상 떨어져 있는 지점에서는 티를 이용하여 샷을 할 복 있다” 라고 규정이 되어 있는 것을 보면 퍼팅 그린과 골프 코스와의 차이가 없었으며 코스의 개념이 현재의 코스와는 심히 달라서 퍼팅이라고 보기는 어려웠다고 보아야 할 것이다.
이후 룰은 1875년에 티잉 그라운드를 지정하여 분리해 내면서 골프는 새로운 국면을 맞이 했다. 이때의 퍼팅은 현재의 칩 샷에 가까웠을 것이다 는 것이 일반적인 생각이다. 현재의 퍼팅에 비해 군 당시에는 퍼팅의 라인이 커다란 의미를 가지는 것이 아니었다.
하지만 현재의 그린을 관리하고 유지하는 기술이 첨단화되어 그린을 다지고 잔디를3/16인치까지 혹은 썩 짧게 깎을 삶 있게 된 것이다. 이는 그린의 스피드를 빠르게 해주었고 퍼팅은 금시 단순히 운이 아니라 기술이 된 것이다.

… PGA 선수의 퍼팅 성공률 …

1. 2 풋 퍼트(foot putt); 얼추 대부분을 성공
2. 3 풋 퍼트; 85-95%성공
3. 5 풋 퍼트; 65%성공
4. 6 풋 퍼트; 50%성공
5. 10 풋 퍼트; 25%
6. 15 풋 퍼트; 10%

-2 feet의 퍼트가 10 feet의 퍼트에 비해 성공률이 월등 높다. 따라서 그린 사이드의 샷이나 롱 퍼팅의 목표는 볼을 2feet의 길바닥 안에 넣는 것이다.

-퍼팅의 이상적인 포워드 롤(forward roll)과 퍼팅 성공률의 높일 복수 있는 퍼팅의 스피드는 홀을 17 인치 계획성 지나가게 친다.

… 그립(grip)의 종류 …

1. 리버스 오버랩 그립(reverse overlap grip)
a. 왼손과 오른손 엄지손가락을 그립의 중심을 향해 잡고 왼손 검지 손가락을 바른손 위로 잡는 방법
b. 왼손등과 오른손 바닥이 퍼터 페이스(putter face)방향과 일치하도록 잡는다
c. 왼팔로 방향을 유지한다-à왼 손목에 꺾임 방지
d. 오른손으로 거리감을 조절-à오른손 잡이에게 권장

2. 오버랩 그립(over lap)
:골프 클럽(golf club)을 잡는 것과 같은 방법으로 왼손 오른손 엄지손가락을 그립의 중심을 향해 잡는다.-à왼 손목의 꺾임을 방지하는 것이 중요하다

3. 크로스 핸디드 그립(cross – handed grip)
:왼손을 오른손보다 내려 잡는 방법
-à왼 손목의 꺾임을 방지할 고갱이 있어 볼의 방향성이 좋아 짧은 퍼팅(short putting)에 유리하며, 잡아당기거나 손목이 꺾이는 골퍼에게 드릴로 사용할 목숨 있다

4. 외타 그립

a. 두 손을 분리시켜 잡는 그립.(split handed grip)
-주로 긴 퍼터(long putter)를 사용할 단시 이용한다.
-오른손이 주(主)가 되어 거리를 조절하는 그립이다.

b. 폴 런얀 그립(Paul Runyan grip)
-퍼팅(putting)의 귀재라는 폴 런얀 선수가 사용한 그립의 형태
-양손바닥이 위로 45도 안쪽을 향하는 그립의 형태.
-두 손의 그립 강도(grip pressure)를 같게 하고 왼 손목의 꺾임을 억제하는 데에 유리하다.
ex) 코리 페이븐, 폴 에이징어

c. 집게 그립 (craw grip; =gator grip)

… 셋 생업 (set up) …

1. 그립(grip) 시(時) 양손을 부드럽고 일정하게 잡는다.

2. 볼의 위치(ball position)는 눈과 수직이 되도록 하며, 퍼터 헤드가 최저 점을 지나는 지점에 볼을 위치.
a. 볼이 지나치게 가까울 정경 – 퍼팅 궤도(path)가 out-to-in이 되기 쉬움
b. 볼이 지나치게 멀 비대발괄 – 퍼팅 궤도(path)가 in-to-out이 되기 쉬움

3. 눈의 정렬은 퍼팅 라인에 대해서 평행이 되도록 한다.

4. 스탠스(stance); 무릎, 힙, 어깨가 퍼팅 라인과 평행이 되도록 한다.

5. 롱 퍼트(long putt)나 바람이 외화 때의 퍼트의 경우에는 스탠스를 얼마 더더욱 넓게 한다.

6. 몸무게의 중심은 발뒤꿈치나 발가락이 아닌 발의 중간에 두며, 체중의 분배(weight distribution)는 스탠스의 중앙이나 어지간히 왼발 쪽에 둔다.

7. 일정한 준비동작(routine)을 만든다.

-루틴(routine)-

1. 볼의 로고(logo)를 퍼팅 라인에 맞춘다.
2. 볼과 퍼팅 라인(putting line) 뒤에 서서 볼이 지나갈 라인(line)과 거리를 상상한다.
3. 터치(touch) 감과 거리감을 익히기 위해 2,3차례 연습스윙을 한다.
4. 볼쪽으로 다가가면서 세려 속으로 퍼팅 라인(putting line)을 기억한다.
5. 실지 볼에서 3-4인치 떨어진 지점에서 가상의 볼을 놓고 셋 업(set up)을 어찌 자신감이 들판 때까지 연습스윙을 한다.
6. 눈을 정렬하고 퍼터 페이스(putter face)와 몸을 정렬한다.
7. 완벽한 스트로크를 한다.

-스트로크 (stroke)-

1. 스트레이트로 스트로크 한다; 연습방법(drill) -à 퍼팅 트랙(putting track) 및 골프 클럽(golf club)을 이용하여 연습한다.

2. 어깨의 회전을 사용해서 추 운동(pendulum)으로 스트로크 한다; 연습방법(drill) -à겨드랑이에 나무막대를 끼우고 문틀 중핵 라인 후(後), 나무막대가 문틀에 부딪히지 않도록 어깨를 아래위로 움직여야 한다. 이렇게 하면 퍼터(putter)가 일직선으로 바르게 움직이는 스트로크(stroke)를 할 핵 있다.

3. 스윗 스팟(sweet spot)에 맞게 스트로크 한다; 연습방법(drill)-à 밴드, 골프 티(tee,) 이쑤시개 등을 퍼터 페이스(putter face)에 부착하여 연습

4. 어깨, 팔, 손에 의해 만들어진 오각형을 유지한 채로 스트로크를 한다; 연습방법(drill) -à 타월이나 클럽을 양쪽 겨드랑이에 끼고 연습한다.

5. 오산 팔꿈치는 자연스럽게 몸에 붙도록 한다.

… 스트로크 형태에 따른 판별 …

1. 스트로크 퍼팅(stroke putting); 어깨, 팔, 손에 의해 만들어진 오각형을 유지한 채로 추 운동(pendulum)으로 스트로크 한다.

2. 터프 퍼팅(turf putting); 오각형 기축 그립(grip)부분을 지점으로 손목을 꺾었다가 때려 주는 방식으로 팔로우 쓰로우(follow throw)를 크게 다리 않는 특징이 있다.

3. 탭 퍼팅(tap putting); 손목만으로 쳐주는 방식으로 홀 컵(hole cup) 앞에서의 짧은 퍼팅 할 적기 쓰인다.

… 퍼팅(putting) 스탠스(stance)의 매가지 …

1.스퀘어 스탠스(square stance)
:양 발이 퍼팅 라인(putting line)에 평행인 스탠스; 쥔님 일반적인 스탠스

2.오픈 스탠스(open stance)
:왼발이 몸의 뒤쪽으로 빠지는 형태의 스탠스; 몸의 정렬도 왼쪽
퍼팅 라인(putting line) 읽기에 용이하다
팔로우 쓰로우(follow through)를 원활하게 해준다
오른쪽 눈이 주시(注視)인 사람에게 유리하다; ex)잭 니클라우스, 밴 크랜쇼

3.클로즈드 스탠스(closed stance)
:오른발을 몸의 뒤쪽으로 빠지는 형태의 스탠스; 몸의 정렬도 오른쪽.
-퍼팅 라인(putting line)을 인-투-아웃(in-to-out)으로 유도할 삶 있어 볼을 아웃-투-인(out-to-in)으로 깎아 치는 골퍼에게 드릴(drill)로 사용할 생명 있다.

… 퍼팅에 대한 과학적인 분석 …

1. 퍼팅에 있어서 쥔어른 적합한 스피드는 볼이 홀을 40-50cm 수평 지나서 멈추는 것이며, 이는 바깥사람 높은 퍼팅의 성공률을 보이며 거기 다음의 퍼팅을 성공하는 데에 있어서도 내적 부담감이 없다.

2. 퍼팅에 있어서 처음의 15%는 볼이 회전을 징검다리 않고 힘껏 미끄러지듯이 가며, 15%이후를 이동하고서야 볼이 회전을 어찌어찌 굴러간다. 볼의 회전이 없이 이동하는 동안에는 잔의 결이나 스파이크 마크 등에 영향을 덜 받지만 볼의 회전이 생기기 시작하는 지점부터는 잔디의 결, 스파이크 노트 등이 볼의 스피드나 볼이 브레이크 하는 정도에 큰 영향을 준다.

3. 볼이 그린 위를 굴러가는 동안에 지면의 경사는 지구의 중력이라는 우사 그리하여 볼에 많은 영향을 미친다. 그린의 스피드가 빠르면 퍼팅을 하는 데에 있어서 강한 스트로크가 필요하지 않아서 초기의 볼의 속도가 느려진다. 이로 인해 볼은 느린 그린에 비해 시작하는 지점부터 볼이 멈추는 지점까지 경과하는 시간이 길어져서 경사와 중력의 지배에 더한층 무진 노출되어 볼의 휘는 정도가 심하다.

4. 퍼터의 센터에 볼을 즉변 타격하지 못하면 이는 볼의 방향과 거리의 조절에 있어서 어려움이 많게 된다. 이는 골프 스윙의 경우와 다를 바 없다고 하겠다.

5. 퍼터의 스피드가 은근슬쩍 많아서 볼이 홀을 3-4피트 크기 지났다면, 볼이 홀의 센터를 지나더라도 볼이 홀에 드랍(drop) 되기는 어렵다.

6. 인위적으로 볼에 포워드 스핀(forward spin)을 넣기 위해 볼을 탑핑 하듯이 치려하는 것은 어려울 뿐만이 아니라, 이는 다다 1foot 정도의 액스트라 롤(roll)만이 더해질 뿐이다.

7. 볼이 홀 컵에 슬그머니 굴러 들어갈 복 있을 정도로 퍼팅을 하는 골퍼는 잔디 추량 반대 방향으로 퍼팅을 하거나 스파이크 노트 위를 지날 경우에는 퍼팅의 출신 확률이 낮아진다.
-à빠른 그린의 벤트 그레스 경우에는 유리하지만 버뮤다 그레스에서는 불리하다.

8. 2/3 이상의 볼의 브레이크(break)는 홀의 3피트 안에서 일어난다.

9. 경사지에서의 15피트의 퍼트는 평지의 30피트 퍼트와 같은 정도의 어려움을 지닌다. 이는 경사지에서의 퍼트에 대한 초기의 볼에 대한 방향의 미스의 마진이 작기 때문이다.

10. 2.5 피트 거리의 퍼트에서 얼라인먼트의 회건 범위는 4도 수평 여민 작은 정도의 미스는 퍼팅의 성공여부에 큰 영향을 주소지 않는다. 또 말하면 길미 거리의 퍼트는 누구나 성공하여야 한다.

… 퍼터에 대한 이번 해 …

-로프트(loft)-

;모든 퍼터에는 로프트 각이 2-7도 정도 있다.

1. 잔디가 짧게 깎인 빠른 그린에서는 로프트 각이 2-4도 전경 놈 것이 유리하다.
-à임팩트 이환 곧바로 볼의 포워드 롤이 생기는 데에 도움이 된다.

2. 잔디가 길어서 느린 그린에서는 로프트의 각이 4-7도 한도 남사 것이 볼이 첫 번째 바운스를 하고 나서 그린 위에서 순수한 포워드(forward) 롤(roll)을 하는 데에 유리하다.

3. 셋 업 시 손목의 꺾이는 정도가 많아서 손의 위치가 왼쪽 허벅지쪽에 위치하는 골퍼의 경우에는 로프트 각을 줄이는 효과를 가져오기 그리하여 로프트 각이 대단히 높은 퍼터를 사용하는 것이 유리하다.

4. 셋 벌잇자리 구음 손의 위치가 스탠스의 중앙에 위치하는 골퍼는 로프트 각이 작은 퍼터를 사용하는 것이 좋다.

-라이 각(lie angle)-

1. 퍼터의 라이 각은 70-76도에 이르며 제조사에 따라 차이가 있다.

2. 클럽을 수직에 가깝게 세워서 셋 업을 하는 골퍼에게는 라이 각이 많은 퍼터가 유리하다.

3. 손을 몸에 가깝게 붙이고 볼을 몸에서 멀찍이 놓고 상체를 무진 구부리는 골퍼에게는 라이 각이 작은 퍼터가 유리하다.

… 그린(green) 읽기 …

:볼(ball)과 홀(hole) 사이에 경사(slope)의 정도를 측정하는 것과 볼과 홀 사이에 잔디의 종류와 결(grain)이 어느 방향으로 누워있는지를 파악하는 것이다. (그린에 올라가기 전에 전체적인 경사를 파악하는 것이 도움이 된다.)

-그린 읽기에 영향을 주는 요소-

1. 잔디 종류에 따라; ex) 벤트 그레스(Bent grass), 블루 그레스(Blue grass), 버뮤다 그레스(Bermuda grass) 등.
2. 바람이 부는 방향에 따라
3. 태양의 방향에 따라
4. 산악지형의 골프장의 사정사정 높은 산의 위치에 따라(mountain break)
5. 바닷가에 인접한 골프장 그린(ocean break)
6. 그린의 스피드(green speed)에 따라
7. 수분의 정도에 따라

-잔디의 결(grain)-

1. 홀 컵(hole cup)의 주위를 보았을 틈새 갈색(흙의 색)을 띄우는 쪽으로 잔디 결이 형성된다. 한쪽은 잔디가 수많이 자라서 색깔이 초록빛이 강하다.

2. 그린 위의 잔디의 색이 보다 반짝이고 파란쪽이 결 방향이다. 잔디 꽁무니 같은 방향으로의 퍼팅의 속도는 불찬 방향으로의 퍼팅에 비해서 스피드가 빠르다. 의견 불가 방향에서 잔디의 색은 반짝임이 덜하다.

3. 잔디의 결은 태양의 방향, 주된 바람의 방향, 물이 있는 방향, 아울러 높은 산의 불찬 방향으로 향한다.

4. 버뮤다 그레스(Bermuda Grass)에서 잔디결의 방향은 벤트 그레스나 블루 그레스에서 더 퍼팅의 스피드에 큰 영향을 미친다.

… 오르막 퍼팅(uphill putt) …

1. 과감한 스트로크(stroke)를 한다.
2. 내리막보다는 경사의 영향을 덜 받는다.
3. 거리감이 중요하다.
4. 볼이 홀 컵을 지나가도록 스트로크를 한다.

… 내리막 퍼팅(downhill putt) …

1. 경사의 영향을 심히 받기 때문에 신후히 고려한다.
2. 퍼터(putter)의 앞쪽(toe)으로 스트로크 한다; à평상시에 연습이 필요하다.
3. 팔로우 쓰로우(follow through)는 짧게 한다.
4. 그립(grip)을 짧게 잡는다. à안정감이 생기고 볼의 속도가 줄어든다.

… 숏 퍼팅(short putting) …

:숏 퍼트(short putt)를 할 때에는 롱 퍼트(long putt)에 비해 몸을 숙이고 스탠스(stance)의 폭을 좁게 어째서 양쪽 팔꿈치를 가슴쪽으로 붙인다.

-연습방법(drill)-
동전(coin)을 이용한 퍼팅 스트로크(putting stroke)

1. 자신감(confidence)을 갖는다.
2. 몸이 경직되지 않게 서적 한 마음으로 스트로크(stroke)를 한다.
3. 손목(wrist)을 쓰지 않는다.
4. 풍경 스윙(back swing)을 작게 한다.
5. 극히 짧은 거리에서는 퍼터 헤드(putter head)를 홀 컵(hole cup)까지 가져가겠다는 마음으로 스트로크(stroke) 한다.

… 롱 퍼팅(long putting) …

;셋업(set up) 시(時) 숏 퍼팅에 비해 상체를 슬그머니 펴주는 자세가 롱 퍼팅에 유리하다; à시야를 확보에 이점(利點)이 있다.

1. 롱 퍼팅의 목적은 홀(hole)에 가깝게 붙이는 것이다.à연습방법: 홀로부터 반경 1m 원안에 넣는 연습
2. 팔로우 쓰로우(follow through)는 크게 어째 퍼터 헤드(putter head)를 가속시킨다. 이는 볼에 포워드(forward) 롤(role)을 준다.
3. 홀(hole)에서부터 시작하여 볼까지 퍼팅 라인(putting line)을 그려본다
4. 손목의 유연성을 조금이용하면 [골프입스](https://acrid-caring.com/sports/post-00029.html) 먼 원자재 퍼트에 유리.
5. 가까운 퍼팅 라인상에 목표를 정하고 볼이 그대 위를 통과하게 한다; -à연습방법: 가까운 퍼팅 라인에 티2개를 꽂아두고 댁 사이로 볼이 지나가게 연습한다.

… 퍼팅에 있어서 소견 명세 …

1. “모든 퍼팅은 직선 퍼트이다” 라고 생각하고 퍼팅을 한다.
-à지면의 경사와 중력이 볼을 브레이크 하게 하므로 충분한 브레이크를 감안하고 퍼팅을 한다.

2. 강한 바람은 퍼팅의 스트로크에 영향을 준다.
-à스탠스를 넓히고 신체의 각을 더 많게 호위호 견고한 자세를 극한 후에 퍼팅을 한다.

3. 볼을 치는 것이 퍼팅의 목적이 되어서는 안되고 스트로크를 하는 도중에 볼이 퍼터에 컨택(contact)이 되어야 한다.

4. 팔로우 스윙이 작아지는 것을 방지하기 위해서는 훈련 스윙의 팔로우 스윙의 크기를 하양 스윙의 2 배가 상당히 한다.

5. 팔로우 스윙 성곡 손이 왼쪽 허벅지를 지나도록 한다.

6. 타봉 헤드를 들고 셋 업을 한다.
a. 손에 의한 스트로크이 아닌 팔과 어깨에 의한 스트로크가 되기 쉽다.
b. 테이크 어웨이 클럽이 잔디를 스치는 것을 공급 할 행운 있다.

7. 골프 공의 모양을 한도 퍼터를 이용하거나 테이프를 사용하여 퍼터 헤드에 동전을 붙여서 퍼팅을 연습하면 볼을 스윙 스팟에 타격할 생목숨 있는 능력이 배양 된다.

8. 그립을 강하게 잡고 스트로크를 빠르게 하는 골퍼에게는 짧고 강한 샤프트가 필요하고, 그립을 가볍게 잡고 스트로크가 길고 부드러운 골퍼에게는 길고 약한 샤프트가 적당하다.

9. 캐디나 같은 팀의 동료가 라인업을 하는 데에 있어서 도움이 될 운 있으므로 필요하면 도움을 요청한다.

10. 티를 이용하여 타깃으로 삼아 퍼팅 연습을 하는 것을 집중력에 도움이 되고, 실상 상황에서 홀 컵이 크게 보이게 하는 정신적인 측면이 작용하여 이점이 있다.

11. 아무러면 스트로크가 완벽하더라도
A. 그린을 오산 읽거나
B. 에임(aim)이 잘못되거나
C. 그린의 상태가 나빠서
-à퍼팅을 실패 할 성명 있다.

12. 클럽 페이스의 에러는 스윙 패스의 에러의 70%정도 퍼팅 라인에 영향을 미친다

… Yips(입스) …

:스윙의 결과에 대한 불안감으로 인해 정상적으로 스트로크를 할 요행 없는 경우; 심한 사연 손이 떨리고 어깨가 경직되며 호흡도 가빠진다.

-입스(Yips)를 극복 할 요행 있는 방법-

1. 좋은 기억들만 생각한다; 열성적 사고; positive mindset
-à다른 경기에 있었던 샷의 좋은 결과를 생각하는 것은 심리적인 부담을 줄여 줄 복수 있다.
-à”샷을 미스하는 일은 있을 요체 있다”고 생각한다. 중요한 퍼팅을 미스하는 것은 삶의 일부라고 받아들인다. 퍼팅의 미스가 절대 일어나지 않도록 노력하는 것이 아니라 미스가 적게 발생하도록 하는 것이 효과적이고 긍정적이다.

2. 퍼팅 스타일(style)을 바꾼다.
-à현 PGA선수인 크리스 디마르코는 퍼팅으로 선수의 생명이 끝날 행운 있는 위기에 처한 추후 그립의 형태를 사람들이 원판 예상치도 못하는 모양(게이터 그립)으로 바꾼 나중 더없이 큰 성공을 거두었다.
-à왼손을 오른손보다 내려 잡는다-à왼 손목의 사용을 억제하는 효과를 낸다.

3. 퍼터를 교체한다.
-à퍼터를 교체하는 데에 있어서는 종류가 대단히 다르거나 무게가 굉장히 달라서 셋 업을 하는 데에 있어서나 스트로크를 하는 데에 있어서 현저한 차이가 나는 것이 효과적이다.
-à롱 퍼터를 사용한다.-à짧은 거리에서 클럽의 흔들림을 방지하는 것이 효과적이다.
-à블레이드 퍼터를 사용한다.-à스트로크이 더더욱 스윙의 아크를 형성한다.

4. 정서적 안정감을 위해 프리 샷(pre-shot) 루틴을 정한다.
a. 퍼팅 라인을 읽는다.
b. 실지 할 스트로크를 연습한다.
c. 셋 업을 한다.
d. 호흡을 한다.
e. 스트로크를 한다.

5. 대단히 짧은 거리의 퍼팅부터 연습을 시작하여 입신양명 확률을 높인다.

… 그린(green)에서의 에티켓(etiquette)과 규칙(rule) …

1. 그린(green)에서는 슬며시 ;동반자나 동반경기자가 퍼팅(putting)을 준비하면 하던 행동을 멈추고 슬그머니 한다. 퍼팅은 심리적인 요소와 크게 영향을 미치므로 주의하여야 한다.

2. 플레이 선상(line of play)에 있지 창 것 ;동반경기자 및 동반자가 경기를 하는 주변에 깨끗이 가깝게 근접하여 있거나, 홀 컵(hole cup)의 이어 뒤에 있는 것은 에티켓에 어긋나는 행위이다. 또한, 자신의 그림자가 동반자의 퍼팅 라인을 방해하지 않도록 주의해야 한다.

3. 볼 마크(ball mark)와 마크의 이동 ;퍼팅 그린 위에 볼이 올라가면 경기자 및 동반자는 변이 내지 이와 유사한 물건으로 볼의 위치를 표시한 사후 집어 올릴 수 있다. 만일 마크하지 않고 집어 올리면 1타(one stroke)의 벌점(penalty)이 부과된다. 볼의 위치를 치부 테두리 동전이 다른 경기자 및 동반자의 플레이에 방해가 될 때에는 클럽헤드 길이만큼 또는 이놈 이상으로 방해가 되지 않는 테두리 내에서 이동 할 운명 있다.

4. 퍼팅 선 위에 클럽을 놓는 동작 ;경기자 및 동반자는 퍼팅 주소지 귀루 퍼터를 볼의 전방에 장소 시킬 길운 있다. 그럼에도 표시를 목적으로 누르거나 잔디에 상처를 또는 못 한다.

5. 퍼팅 라인(putting line) 위에 갈무리 할 핵 있는 것과 없는 것 ;퍼팅 라인 위에 볼의 낙하로 인하여 만들어진 볼 자국이나 과거의 홀 순위 자국은 조처 할 길운 있다. 오히려 경기자 및 동반자들의 스파이크 자국을 처변 할 목숨 없다. 만약 이를 수리하면 2타의 벌점(2 stroke penalty)이 부과된다. 퍼팅 라인에 놓여 있는 가장이 혹은 수엽 등 루즈 임페디먼트(loose impediment)는 치울 호운 있다.

6. 퍼팅 그린 검사하기 ;플레이를 하는 경기자 및 동반자는 퍼팅 그린 위에서 볼을 굴리거나, 그린 면을 문지르거나 긁어 그린의 상태를 실험 할 명맥 없다.

7. 퍼팅 라인 밟고 소재지 하는 것 ;경기자 및 동반자는 퍼팅 그린 위에서 퍼팅 라인 또는 볼의 뒤 연장선 위를 밟고 서서 스트로크 단계 못한다.

8. 다른 경기자 및 동반자의 라인을 밟는 행동 ;경기자는 볼을 표 하거나 퍼팅을 위해 그린을 검사할 하저 다른 경기자 및 동반자의 퍼팅 라인 밟지 말아야 한다.

9. 다른 볼이 정지한 이다음 사회운동 전결 ;경기자 및 동반자는 퍼팅 그린 위에서 스트로크한 다른 플레이어 볼이 움직이고 있는 나간 스트로크 다리깽이 말아야 한다.

10. 깃대(flagstick) 또는 그린 위의 다른 공을 맞힌 사정사정 ;경기자가 퍼팅한 볼이 그린 위에 놓여 있는 다른 볼을 맞혔을 때는 2타의 벌점이 부과된다. 홀에 꽂혀 있는 깃대 또는 그린 위에 놓여 있는 깃대에 볼이 닿아도 역시 2타의 벌점이 부과된다. 더구나 볼이 놓여져 있는 댁네 지점에서 경기는 재개된다.
