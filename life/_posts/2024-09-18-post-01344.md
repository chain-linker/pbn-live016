---
layout: post
title: "[ Mastering Bitcoin ] 06. 비트코인 트랜잭션"
toc: true
---

 원문 : https://github.com/bitcoinbook/bitcoinbook
 

 * 복습할 때 정리했던 기지 업로드 합니다. 중요하지 않은 부분은 제외하고 개념과 노하우 위주로 정리했어요.
 ​
 ​

## 트랜잭션
 Transactions
 are data structures that encode the transfer of value between participants in the Bitcoin system.
 비트코인 사회 이용자 간의 자석 이전을 암호화하는 데이터 구조
 ​
 Transaction outputs consist of two parts
 1) An amount of bitcoin, denominated in satoshis, the smallest bitcoin unit
 비트코인의 양, 양반 작은 비트코인 단위
 2) A cryptographic puzzle that determines the conditions required to spend the output
 소유권 증명을 위한 암호학적 퍼즐
 ​
 The input contains four elements
 1) A transaction ID, referencing the transaction that contains the UTXO being spent
 UTXO가 사용된 트랜잭션을 참조하는 트랜잭션 ID
 2) An output index (vout), identifying which UTXO from that transaction is referenced
 견련 트랜잭션의 UTXO에 대한 소유권을 증명 (첫번째는 0에서 시작)
 3) A scriptSig, which satisfies the conditions placed on the UTXO, unlocking it for spending
 scriptSig로 잠금 스크립트를 해제
 4) A sequence number (to be discussed later)
 시퀀스 번호
 ​
 

 ​

## 스크립트 구조
 Script Construction
 Combining scriptSig and scriptPubKey to evaluate a transaction script is an example of the unlocking and locking scripts for the most common type of bitcoin transaction [비트겟](https://lunchfar.link/life/post-00119.html) (a payment to a public key hash), showing the combined script resulting from the concatenation of the unlocking and locking scripts prior to script validation.
 ​
 

 ​
 ​
 Example
 ​
 ​
 작동방식 - 타입이 데이터라면 스택에 삽입을 하고 명령어라면 당신 명령을 실행한다.
 ​
 1) 2는 데이터이므로 스택에 삽입
 2) 3도 데이터이므로 스택에 삽입
 3) ADD는 명령어이므로 2와 3을 더하는 명령 실시 → 5
 4) 5는 데이터이므로 스택에 삽입
 5) EQUAL은 명령어이므로 5(2 ADD 3)와 5가 같은지 비교하는 명령 천행 → TRUE
 ​
 ​
 1) Sig는 데이터이므로 스택에 삽입
 2) PubK도 데이터이므로 스택에 삽입
 3) DUP은 명령어이므로 PubK을 복사하는 명령 천언 → PubK
 4) HASH160는 명령어이므로 스택 맨 착임 항목을 연산 차기 스택에 삽입 → PubKHash
 5) PubKHash는 데이터이므로 스택에 삽입
 6) EQUALVERIFY는 명령어이므로 PubKHash와 PubKHash가 같은지를 비교하는 명령 실시 → 같으면 제거
 7) CHECKSIG는 명령어이므로 서명 sig를 PubK와 매치해 검사하고 같은지를 비교하는 명령 친행 → TRUE
 ​
 ​
 Signature Hash Types (SIGHASH)
 ​
 

